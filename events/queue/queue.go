package queue

import "github.com/streadway/amqp"

const (
	Transient DeliveryMode = 1 + iota
	Persistent
)

type DeliveryMode uint8

type Options struct {
	Url             string
	ConnectAttempts int
	Exchange        struct {
		Name    string
		Type    string
		Durable bool
	}
}

type Publishing interface {
	GetBody() ([]byte, error)
	GetHeaders() amqp.Table
	SetHeaders(map[string]interface{})
	GetDeliveryMode() DeliveryMode
	GetRoutingKey() string
}
