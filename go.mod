module gitlab.com/go-lang-tools/tools

go 1.18

require (
	gitlab.com/go-lang-tools/events v0.0.1
	gitlab.com/go-lang-tools/i18n v0.0.1
	github.com/Arrim/jaeger-gin v0.0.2
	github.com/archdx/zerolog-sentry v0.0.1
	github.com/fvbock/endless v0.0.0-20170109170031-447134032cb6
	github.com/getsentry/sentry-go v0.11.0
	github.com/gin-gonic/gin v1.7.4
	github.com/go-redis/redis/v8 v8.3.4
	github.com/gobuffalo/envy v1.10.2
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/google/jsonapi v1.0.0
	github.com/google/uuid v1.3.0
	github.com/gookit/event v1.0.5
	github.com/jackc/pgtype v1.13.0
	github.com/jackc/pgx/v5 v5.2.0
	github.com/nicksnyder/go-i18n/v2 v2.2.0
	github.com/onsi/ginkgo v1.14.2
	github.com/onsi/gomega v1.10.3
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.28.0
	github.com/streadway/amqp v1.0.0
	golang.org/x/text v0.3.8
)

require (
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90 // indirect
)
