package amqp_v2

import (
	"gitlab.com/go-lang-tools/events"
	"gitlab.com/go-lang-tools/tools/repositories"
)

func InitializeEventRepository(rep *repositories.Repositories) *events.DBRepository {
	return events.NewDBRepository(rep, "events")
}

func InitializeEventBus(client events.Publisher, rep *events.DBRepository) *events.EventBus {
	return events.NewEventBus(client, rep)
}
