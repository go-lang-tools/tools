package amqp_v2

import (
	"github.com/gobuffalo/envy"
	"gitlab.com/go-lang-tools/events/kafka"
)

const (
	KafkaBrokersEnv = "KAFKA_BROKERS"
)

func InitializeKafka() *kafka.Kafka {
	return kafka.NewKafka(envy.Get(KafkaBrokersEnv, ""))
}

func InitializeKafkaPublisher(client *kafka.Kafka) (*kafka.Producer, error) {
	err := client.RunProducer()
	if err != nil {
		return nil, err
	}

	return kafka.NewProducer(client), nil
}
