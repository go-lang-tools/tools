package main

import (
	errors2 "errors"
	"fmt"

	"gitlab.com/go-lang-tools/tools/helpers/errors"
)

func main() {
	berr := errors2.New("base")
	err := errors.Wrap(berr, "Test")
	errCode := errors.AddErrorCode(err, 100)
	fmt.Println(errCode.Error())

	if errors2.Is(errCode, berr) {
		fmt.Println("Done")
	}
}
